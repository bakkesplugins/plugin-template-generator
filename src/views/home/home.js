const {remote, ipcRenderer} = require("electron");

if (remote.getCurrentWindow().env === 'dev') {
    window.baseDirRequire = '../../';
    window.baseDir = './src/'
} else {
    window.baseDirRequire = '../../';
    window.baseDir = './resources/app/'
}

ipcRenderer.on('menu-bar-credits', function() {
    let message = `
        <p>
            Templates originally provided by:<br>
            &nbsp;&nbsp;&nbsp;&nbsp;Basic - CinderBlock<br>
            &nbsp;&nbsp;&nbsp;&nbsp;Imgui - Stanbroek
        </p>
        <p>Icon provided by: Freepik</p>
    `;
    showGenericModal('Credits', message);
});

///---------------------------------------------------------------------------------------------------------------------
const fs = require('fs');
const child_process = require('child_process');
const homedir = require('os').homedir();
const regedit = require(window.baseDirRequire + 'node_modules/regedit/index.js');

let bm_sdk_textbox;
let plugin_name_textbox;
let transformed_plugin_name_display;
let plugin_description_textbox;
let plugin_type_select;
let plugin_save_location_textbox;
let template_type_select;
let attempt_start_checkbox;
let generate_button;
let reset_form_button;
let generic_close_modal_button;

const pluginTypeMap = [
    "PLUGINTYPE_BOTAI",
    "PLUGINTYPE_CUSTOM_TRAINING",
    "PLUGINTYPE_FREEPLAY",
    "PLUGINTYPE_REPLAY",
    "PLUGINTYPE_SPECTATOR",
    "PLUGINTYPE_THREADED"
];

const templateTypeMap = [
    'basic',
    'imgui'
];
document.addEventListener('DOMContentLoaded', function () {
    bm_sdk_textbox = document.getElementById('bm_sdk_location');
    plugin_name_textbox = document.getElementById('plugin_name');
    transformed_plugin_name_display = document.getElementById('transformed_plugin_name_display');
    plugin_description_textbox = document.getElementById('plugin_description');
    plugin_type_select = document.getElementById('plugin_type');
    plugin_save_location_textbox = document.getElementById('plugin_save_location');
    template_type_select = document.getElementById('template_type');
    attempt_start_checkbox = document.getElementById('attempt_start');
    generate_button = document.getElementById('generate_form');
    reset_form_button = document.getElementById('reset_form');
    generic_close_modal_button = document.getElementById('generic_message_modal_close_button');

    generate_button.onclick = function () {
        generateFormSubmit();
    };
    reset_form_button.onclick = function () {
        window.location.href = "";
    };

    plugin_name_textbox.onkeyup = function (ev) {
        plugin_name_KeyUp(ev);
    };

    generic_close_modal_button.onclick = function () {
        let genericModal = document.getElementById('generic_message_modal');
        genericModal.classList.remove('is-active');
    };

    regedit.list('HKCU\\Software\\BakkesMod\\AppPath', function (err, result) {
        if (result.hasOwnProperty('HKCU\\Software\\BakkesMod\\AppPath')
            && result["HKCU\\Software\\BakkesMod\\AppPath"].hasOwnProperty('values')
            && result["HKCU\\Software\\BakkesMod\\AppPath"].values.hasOwnProperty('BakkesModPath')
            && result["HKCU\\Software\\BakkesMod\\AppPath"].values["BakkesModPath"].hasOwnProperty('value')
        ) {
            bm_sdk_textbox.value = result["HKCU\\Software\\BakkesMod\\AppPath"].values["BakkesModPath"].value + "bakkesmodsdk\\";
        }
    });
    plugin_save_location_textbox.value = homedir + "\\Documents\\BakkesModPlugins\\";
});

let isGenerating = false;
function generateFormSubmit() {
    isGenerating = true;
    let loaderOverlay = document.getElementById('loader_overlay');
    let errorList = [];
    loaderOverlay.classList.remove('is-hidden');

    setTimeout(function() {
        let sdkPath = bm_sdk_textbox.value;
        if (sdkPath.substr(-1) !== '\\') {
            sdkPath += '\\';
        }

        let pluginName = transformPluginName();
        let pluginDescription = plugin_description_textbox.value;
        let pluginType = plugin_type_select.selectedIndex;
        let templateType = template_type_select.selectedIndex;
        let attemptStartProject = attempt_start_checkbox.checked;

        let validDescRegex = /[a-z0-9. \-_]+/i;
        let validNameRegex = /^[a-z][a-z0-9_]+$/;
        if (!fs.existsSync(sdkPath)) {
            errorList.push("Selected BakkesMod SDK Path does not exist");
        } else {
            if (!fs.existsSync(sdkPath + "lib")) {
                errorList.push("Invalid SDK folder. Could not find 'lib' sub-folder");
            }
            if (!fs.existsSync(sdkPath + "include")) {
                errorList.push("Invalid SDK folder. Could not find 'include' sub-folder");
            }
        }
        if (pluginName.length < 1) {
            errorList.push("Plugin Name is required");
        } else if (pluginName.length > 40) {
            errorList.push("Plugin Name must be 40 characters or less");
        } else if (!validNameRegex.test(pluginName)) {
            errorList.push("Plugin Name must start with a letter and only contain letters, numbers, and underscores");
        }
        if (pluginDescription.length < 1) {
            errorList.push("Plugin Description is required");
        } else if (pluginDescription.length > 60) {
            errorList.push("Plugin Description must be 60 characters or less");
        } else if (!validDescRegex.test(pluginDescription)) {
            errorList.push("Plugin Description can only contain Alphanumeric, period, space, hyphen, and underscore characters");
        }
        if (!(pluginType > 0 && pluginType < 7)) {
            errorList.push("A Plugin Type must be selected")
        } else {
            pluginType = pluginTypeMap[pluginType-1];
        }
        if (!(templateType > 0 && templateType < 3)) {
            errorList.push("A Template Type must be selected")
        } else {
            templateType = templateTypeMap[templateType-1];
        }

        if (errorList.length > 0) {
            showGenericModal("Missing Fields", errorList.join("<br>"));
        } else {
            let dirSplit = plugin_save_location_textbox.value.split("\\");
            let dir = "";
            dirSplit.forEach(function(part) {
                if (part.length < 1) {
                    return;
                }

                dir += part + "\\";
                if (!fs.existsSync(dir)) {
                    fs.mkdirSync(dir);
                }
            });
            dir += pluginName + "\\";
            if (!fs.existsSync(dir)) {
                fs.mkdirSync(dir);
            }

            let pluginNamePascal = "";
            pluginName.split("_").forEach(function(p) {
                pluginNamePascal += p.charAt(0).toUpperCase() + p.slice(1);
            });
            let imguiMenuTitle = "";
            if (pluginType === 'imgui') {
                imguiMenuTitle = pluginNamePascal.replace('_', ' ');
            }

            let templateBase = fs.realpathSync(window.baseDir + "static/plugin_template/" + templateType);
            let files = directoryWalkSync(templateBase);
            files.forEach(function(file) {
                let localizedPath = file.substr(templateBase.length+1);
                let content = fs.readFileSync(file, { encoding: 'utf8' });
                if (localizedPath === 'plugin_window_header\\pluginwindow.h') {
                    if (!fs.existsSync(sdkPath + "include\\bakkesmod\\plugin\\pluginwindow.h")) {
                        fs.writeFileSync(sdkPath + "include\\bakkesmod\\plugin\\pluginwindow.h", content);
                    }

                    return;
                }

                content = content
                    .replace(/{{PLUGIN_NAME_FIXED}}/g, pluginName)
                    .replace(/{{PLUGIN_DESCRIPTION}}/g, pluginDescription)
                    .replace(/{{PLUGIN_TYPE}}/g, pluginType)
                    .replace(/{{IMGUI_MENU_NAME}}/g, imguiMenuTitle)
                    .replace(/{{PLUGIN_NAME_PASCAL_CASED}}/g, pluginNamePascal)
                    .replace(/{{BM_SDK_BASE_PATH}}/g, sdkPath);
                let newName = localizedPath.replace(/PLUGIN_NAME_FIXED/g, pluginName);

                let dir2 = dir;
                let folderCheck = newName.split('\\');
                folderCheck.slice(0, folderCheck.length-1).forEach(function(part) {
                    if (part.length < 1) {
                        return;
                    }
                    dir2 += part + "\\";
                    if (!fs.existsSync(dir2)) {
                        fs.mkdirSync(dir2);
                    }
                });
                fs.writeFileSync(dir + "\\" + newName, content);
            });

            if (attemptStartProject) {
                setTimeout(function() {
                    child_process.exec('start "" "'+ dir + pluginName + '.vcxproj"',  function() {});
                }, 2000);
            }

            let finishMessage = `
                <p>We've finished generating your template!

                    If you're <b>NOT</b> using Visual Studio 2019, you will need to change your Platform Toolset:
                </p>
                <ol>
                    <li>Have the project open in Visual Studio</li>
                    <li>On the very top menu bar with "File" and "Edit", click "Project" -> "Properties"</li>
                    <li>Change the Platform Toolset to the option that applies to your current Visual Studio version</li>
                </ol>
                <img alt="Settings Example" src="../../static/images/settings_to_change.png">
            `;
            showGenericModal("Template finished generating!", finishMessage);
        }
        loaderOverlay.classList.add("is-hidden");
        isGenerating = false;
    }, Math.floor(Math.random() * 2000) + 1000); //Sue me -- https://www.fastcompany.com/3061519/the-ux-secret-that-will-ruin-apps-for-you
}
function plugin_name_KeyUp() {
    transformed_plugin_name_display.innerHTML = transformPluginName();
}
function transformPluginName() {
    let name = plugin_name_textbox.value;
    name = name.toLowerCase();
    name = name.replace(/[^a-z0-9_\- ]+/g, '');
    name = name.replace(/[\- ]+/g, '_');
    return name;
}
function directoryWalkSync(dir) {
    let results = [];
    let list = fs.readdirSync(dir);
    list.forEach(function(file) {
        file = dir + '/' + file;
        let stat = fs.statSync(file);
        if (stat && stat.isDirectory()) {
            results = results.concat(directoryWalkSync(file));
        } else {
            results.push(fs.realpathSync(file));
        }
    });
    return results;
}

function showGenericModal(title, message) {
    let genericModal = document.getElementById('generic_message_modal');
    let genericModalTitle = document.getElementById('generic_message_modal_title');
    let genericModalContent = document.getElementById('generic_message_modal_content');

    let wrap = message.substr(0, 1) !== "<";

    genericModalTitle.innerText = title;
    genericModalContent.innerHTML = (wrap ? "<p>" : "") + message + (wrap ? "</p>" : "");
    genericModal.classList.add('is-active');
}