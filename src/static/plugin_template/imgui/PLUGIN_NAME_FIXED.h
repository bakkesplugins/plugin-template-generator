#pragma once
#pragma comment(lib, "BakkesMod.lib")
#include "bakkesmod/plugin/bakkesmodplugin.h"
#include "imgui/imgui.h"

class {{PLUGIN_NAME_PASCAL_CASED}} : public BakkesMod::Plugin::BakkesModPlugin, public BakkesMod::Plugin::PluginWindow
{
public:
	virtual void onLoad();
	virtual void onUnload();

/* GUI Functions */
private:
	bool isWindowOpen = false;
	bool isMinimized = false;
	std::string menuTitle = "{{IMGUI_MENU_NAME}}";
public:
	virtual void Render();
	virtual std::string GetMenuName();
	virtual std::string GetMenuTitle();
	virtual void SetImGuiContext(uintptr_t ctx);
	virtual bool ShouldBlockInput();
	virtual bool IsActiveOverlay();
	virtual void OnOpen();
	virtual void OnClose();
};
