#include "{{PLUGIN_NAME_FIXED}}.h"

void {{PLUGIN_NAME_PASCAL_CASED}}::Render()
{
	//========================================================================================================
	//                                      Your code starts here!
	//========================================================================================================
}

std::string {{PLUGIN_NAME_PASCAL_CASED}}::GetMenuName()
{
	return "{{IMGUI_MENU_NAME}}";
}


std::string {{PLUGIN_NAME_PASCAL_CASED}}::GetMenuTitle()
{
	return menuTitle;
}

void {{PLUGIN_NAME_PASCAL_CASED}}::SetImGuiContext(uintptr_t ctx)
{
	ImGui::SetCurrentContext(reinterpret_cast<ImGuiContext*>(ctx));
}

bool {{PLUGIN_NAME_PASCAL_CASED}}::ShouldBlockInput()
{
	return false;
}

bool {{PLUGIN_NAME_PASCAL_CASED}}::IsActiveOverlay()
{
	return true;
}

void {{PLUGIN_NAME_PASCAL_CASED}}::OnOpen()
{
	isWindowOpen = true;
}

void {{PLUGIN_NAME_PASCAL_CASED}}::OnClose()
{
	isWindowOpen = false;
}
