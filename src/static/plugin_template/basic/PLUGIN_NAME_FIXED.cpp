#include "{{PLUGIN_NAME_FIXED}}.h"
#include "bakkesmod\wrappers\includes.h"

using namespace std;

BAKKESMOD_PLUGIN({{PLUGIN_NAME_PASCAL_CASED}}, "{{PLUGIN_DESCRIPTION}}", "1.0", {{PLUGIN_TYPE}})

void {{PLUGIN_NAME_PASCAL_CASED}}::onLoad() {
    //========================================================================================================
    //                                      Your code starts here!
    // When you're all finished, submit it on https://bakkesplugins.com/ to share it with the world :D
    //========================================================================================================
}
void {{PLUGIN_NAME_PASCAL_CASED}}::onUnload() {

}