### Developing
Electron is finnicky sometimes, make sure everything works both in development builds AND distribution builds before submitting a merge request

1. Run `npm i` in the root directory
2. Use `npm run start` to start the development build
3. Use `npm run dist` to build the distribution files. They output to `built/output`